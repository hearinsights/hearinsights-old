# log/urls.py
from django.conf.urls import url
from . import views
from mainapp import views as mainapp_views

# We are adding a URL called /home
urlpatterns = [
    url(r'^$', views.upload, name='upload'),
    url(r'^upload', views.upload, name='upload'),
    url(r'^set_keywords', views.set_keywords, name='set_keywords'),
    url(r'^recording/(?P<recording_id>[0-9]+)', views.recording, name='recording'),
    url(r'^_add_numbers', views.add_numbers, name='add_numbers'),
    url(r'^processing', views.recording, name='processing')  
]