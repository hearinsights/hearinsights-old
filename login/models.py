from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from django.utils.encoding import python_2_unicode_compatible
from django.utils import timezone

from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User


class Client(models.Model):

    client_name = models.CharField(max_length=200)

    def __str__(self):
        return self.client_name

class Company(models.Model):

    date_modified = models.DateTimeField(auto_now=True)
    custom_keyword = models.TextField(null=True)
    company_name = models.CharField(max_length=200)
    stages = models.TextField(null=True)

    def __str__(self):
        return self.custom_keyword

class Audio(models.Model):

    file_url = models.TextField()
    s3_location_path = models.TextField(null=True)

    upload_time = models.DateTimeField('date published', auto_now=True)

    keyword = JSONField(null=True)
    transcript = JSONField(null=True)
    diarization = JSONField(null=True)
    speaker_label = JSONField(null=True)

    voicebase_raw_response = JSONField(null=True)
    IBM_raw_response = JSONField(null=True)

    client_stage = models.CharField(max_length=200)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE)
    
    # def __str__(self):
    #     return self.file_location
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

