import json

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from login.forms import Custom_keywordForm, AudioForm
from login.models import Company, Audio, Client
from datetime import datetime
from django.contrib.auth.models import User

# Create your views here.
# this login required decorator is to not allow to any  
# view without authenticating

@login_required(login_url="/login/")
def set_keywords(request):

    if request.method == 'POST':
        Custom_keyword_form = Custom_keywordForm(request.POST)

        if Custom_keyword_form.is_valid():
            custom_keyword = request.POST.get('custom_keyword', '')

            try:
                custom_keyword_obj = Company.objects.reverse()[0]
                custom_keyword_obj.custom_keyword = custom_keyword
                custom_keyword_obj.date_modified = datetime.now()
                custom_keyword_obj.save()

            except:
                custom_keyword_obj = Company(custom_keyword=custom_keyword)
                custom_keyword_obj.save()
        
        updated_keywords = Company.objects.reverse()[0]
        return render(request, 'set_keywords.html', {'updated_keywords': updated_keywords,'Custom_keyword_form': Custom_keyword_form})

    else: 
        
        try:
            updated_keywords = Company.objects.reverse()[0]
            data = {'custom_keyword': updated_keywords}
            Custom_keyword_form = Custom_keywordForm(initial=data)
        except:
            updated_keywords = ''
            data = {'custom_keyword': updated_keywords}
            Custom_keyword_form = Custom_keywordForm(initial=data)  

        return render(request, 'set_keywords.html', {'updated_keywords': updated_keywords,'Custom_keyword_form': Custom_keyword_form})



@login_required(login_url="/login/")
def upload(request):
    user_id = request.user
    client_list = list(Client.objects.values_list('client_name', flat=True))

    if request.method == 'POST':
        Audio_form = AudioForm(request.POST)
        if Audio_form.is_valid():
            file_url = request.POST.get('file_url', '').strip()
            client_stage = request.POST.get('client_stage', '')
            client_name = request.POST.get('client_name', '')

            try:
                Client_obj = Client.objects.get(client_name=client_name)
            except:
                Client_obj = Client(client_name=client_name)
                Client_obj.save()

            Audio_obj = Audio(file_url=file_url, client_stage=client_stage, client_id=Client_obj, upload_time=datetime.now(), user_id=user_id)
            Audio_obj.save()
            Audio_form = AudioForm()
        return render(request, 'upload.html', {'Audio_form': Audio_form, 'is_uploaded':True, 'client_list': client_list})
    else: 
        Audio_form = AudioForm()
        return render(request, 'upload.html', {'Audio_form': Audio_form, 'client_list': client_list})


@login_required(login_url="/login/")
def recording(request, recording_id):
    try:
        audio = Audio.objects.get(pk=int(recording_id))
        keyword = json.loads(audio.keyword)['keywords']

        transcript = audio.transcript
        upload_time = audio.upload_time
        diarization = json.loads(audio.diarization)['lines']
        file_url = audio.file_url
        user_name = audio.user_id.username
        client_name = audio.client_id.client_name
        client_stage = audio.client_stage
        audio_id = int(recording_id)

        return render(request, "recording.html", { 'keyword': json.dumps(keyword), 'transcript': transcript, 
            'upload_time': upload_time, 'diarization': json.dumps(diarization), 'file_url': file_url,
            'user_name': user_name, 'client_name': client_name, 'client_stage': client_stage, 'audio_id': audio_id })
    except:
        return render(request, 'processing.html')


def add_numbers(request):
    b = request.GET.get('b','')

    keyword_input = str(request.GET.get('a',''))
    command = ''

    if keyword_input != '':
        keyword_input = keyword_input.split(' ')

        final_command = """ 

            WITH transcript AS (
                SELECT id, jsonb_array_elements((transcript -> 'transcript')) AS line
                FROM login_audio
                Where id ="""+ b +"""            )
            SELECT id, line ->> 'w' AS word, line ->> 's' AS start
            FROM transcript
            WHERE line ->> 'w' ILIKE ANY (ARRAY """ + str(keyword_input) +""");

        """

        final = list(Audio.objects.raw(final_command))

        finaldic = {}
        for i in final:
            if i.word.lower() not in finaldic:
                finaldic[i.word.lower()] = [int(i.start)]
            elif i.word.lower() in finaldic:
                finaldic[i.word.lower()].append( int(i.start) )
        return JsonResponse({ 'result': finaldic })
    else:
        return JsonResponse({ 'result': {} })
