import time
import requests
import boto3

#delete flac file 
import os

#only used for post media hack
import StringIO
import json
import pycurl

#get large file
import shutil
# import os.path #if file exists

#unique file name generation
import uuid

#traceback error printing
import sys
import traceback
import errno

from threading import Thread
from subprocess import Popen, PIPE

from django.db.models import Q

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hearinsights.settings")
import django
django.setup()
from hearinsights import settings
from login.models import Company, Audio

'''Class created to call required voicebase apis'''
class VoiceBase():

    token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIzZGZjNDAzOS1iN2QwLTQ4NGQtOWEzYS02NTQzYjNiYjZlMjUiLCJ1c2VySWQiOiJhdXRoMHw1ODJhNTUyODRkYjYzNDVkNjI3MjMzY2IiLCJvcmdhbml6YXRpb25JZCI6IjNiYWQwYjc5LTE0NjktN2U0Ni02YmIxLWNlY2I3YWRhOWU5MCIsImVwaGVtZXJhbCI6ZmFsc2UsImlhdCI6MTQ3OTYxOTQzMTE0MCwiaXNzIjoiaHR0cDovL3d3dy52b2ljZWJhc2UuY29tIn0.livSuFxAPPVY1xwxi4azFWkOQEM59Vde_ZXG-MF93wk"

    def getDefaultHeaders(self):
        return { "Authorization" : self.token }

    #Todo we should remove having a default vocabName
    def postMediaCurl(self, filepath, vocabName="heap_vocabTest", keywordName=None):
        #name = 'heap_vocabTest'
        url = "https://apis.voicebase.com/v2-beta/media"
        #filepath = 'sampleConvo.mp3'
        
        #TODO figure out how to add groups
        # TODO create checks for valid vocabs and keywords dicts
        if keywordName is not None:
            configurationFormat = '{{"configuration": {{ "keywords": {{"groups": ["{keywordName}"]}}, "transcripts": {{"engine": "premium","vocabularies":[{{"name":"{vocabName}"}}]}}}}}}'
            configuration = configurationFormat.format(vocabName=vocabName, keywordName=keywordName)
        else:
            #old format
            configurationFormat = '{{"configuration": {{"transcripts": {{"engine": "premium","vocabularies":[{{"name":"{vocabName}"}}]}}}}}}'
            configuration = configurationFormat.format(vocabName=vocabName)
        print configuration

        #curly stuff
        storage = StringIO.StringIO()
        c = pycurl.Curl()
        formData = [("media", (c.FORM_FILE, filepath)), ('configuration', configuration)]
        c.setopt(c.POST, 1)
        c.setopt(c.URL, url)
        c.setopt(c.HTTPHEADER, ['{0}:{1}'.format('Authorization', self.token)])
        c.setopt(c.HTTPPOST, formData)
        c.setopt(c.WRITEFUNCTION, storage.write)
        c.perform()
        response_code = c.getinfo(pycurl.RESPONSE_CODE)
        response = storage.getvalue()
        print response
        if response_code != 200:
            print "something gone wrong during media upload"
            raise Exception("somethings gone wrong during media upload")
        return json.loads(response)

    def getMediaJob(self, mediaId):
        #mediaId = '31900b01-af6b-484c-b409-060b13a0048d'
        url = "https://apis.voicebase.com/v2-beta/media/{0}"
        headers = self.getDefaultHeaders()
        r = requests.get(url.format(mediaId), headers=headers)
        print "response was: ", r.status_code #, r.text
        if r.status_code != 200:
            print "somethings gone wrong"
            raise Exception("get media status went wrong")
        responseJson = json.loads(r.text)
        print responseJson.keys()
        jobStatus = responseJson['media']['status'] 
        return (jobStatus, responseJson)

    def uploadVocab(self, name, newVocabWords):
        url = "https://apis.voicebase.com/v2-beta/definitions/transcripts/vocabularies/{0}".format(name)
        headers = self.getDefaultHeaders()
        headers["Content-Type"] = "application/json"
        datum = '''
            {{
                "vocabulary" : {{
                    "name": "{0}",
                    "terms": {1}
                }}
            }}'''
        datum = datum.format(name, json.dumps(newVocabWords))
        r = requests.put(url, headers=headers, data=datum)

        print "response was: ", r.status_code, r.text
        if r.status_code != 200:
            raise Exception("upload vocab went wrong")
            print "somethings gone wrong"

    def uploadCustomKeyword(self, name, newCustomKeywords):
        url = "https://apis.voicebase.com/v2-beta/definitions/keywords/groups/{0}".format(name)
        headers = self.getDefaultHeaders()
        headers["Content-Type"] = "application/json"
        datum = '''
            {{
                "name": "{0}",
                "keywords": {1}
            }}'''
        datum = datum.format(name, json.dumps(newCustomKeywords))
        r = requests.put(url, headers=headers, data=datum)

        print "response was: ", r.status_code, r.text
        if r.status_code != 200:
            raise Exception("upload custom keyword went wrong")
            print "somethings gone wrong"


class Watson():
    username = "ebdf148a-8df2-46d8-a0c6-2631ca3be437"
    password = "D8zs2wK8OrMj"

    def postMedia(self, filepath):
        url = "https://stream.watsonplatform.net/speech-to-text/api/v1/recognize?continuous=true&model=en-US_NarrowbandModel&speaker_labels=true"
        headers = { "Content-Type": "audio/flac" }
        print "starting upload to watson", time.ctime()
        with open(filepath, 'rb') as payload:
            r = requests.post(url, data=payload, headers=headers, auth=(self.username, self.password))
        print "get response back from watson", time.ctime()
        print r.status_code
        if r.status_code != 200:
            print "somethings gone wrong"
            raise Exception("get media status went wrong")
        responseJson = json.loads(r.text)
        return responseJson

    def convertMp3ToFlac(self, mp3Path):
        flacPath = mp3Path.replace('.mp3', '.flac')
        #what if path already exists
        p = Popen(['ffmpeg', '-y', '-f', 'mp3', '-i', mp3Path, '-write_xing', '0', '-f', 'flac', flacPath], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        print stdout, stderr
        return flacPath

    def getJobStatus(self, jobId):
        url = "https://stream.watsonplatform.net/speech-to-text/api/v1/recognitions/{0}".format(jobId)
        r = requests.get(url, auth=(self.username, self.password))
        print r.status_code, r.text
        if r.status_code != 200:
            print "somethings gone wrong"
            raise Exception("get media status went wrong")
        responseJson = json.loads(r.text)
        print responseJson['status']
        return responseJson


'''Get list of current missing audiofile infos'''
def findIncompleteAudioFileInfoIds():
    incompleteAudios = Audio.objects.filter(
        #requires api get
        Q(IBM_raw_response=None) |
        Q(voicebase_raw_response=None) |
        #requires post proccesing
        Q(transcript=None) |
        Q(diarization=None) |
        Q(keyword=None)
    )
    ids = [x.id for x in incompleteAudios]
    return ids


def processIncompleteAudio(audio_id):
    try:
        audioFileInfo = Audio.objects.get(pk=audio_id)
        localLocation = guaranteeFileAtLocation(audioFileInfo)
        transcriptionThread = Thread(target=setTranscript, args=(audioFileInfo, localLocation))
        diarizationThread = Thread(target=setDiarization, args=(audioFileInfo, localLocation))
        transcriptionThread.start()
        diarizationThread.start()
        transcriptionThread.join()
        diarizationThread.join()
        audioFileInfo.save()
        setPostProcessedTranscript(audioFileInfo)
        print "finished processing audio file id ", audio_id
    except:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
        print ''.join('!! ' + line for line in lines)  # Log it or whatever here


def processIncompleteAudioFiles():
    #consider doing this one session at a time instead of all at once
    incompleteAudioFileInfoIds = findIncompleteAudioFileInfoIds()
    print "Number of incomplete files found", len(incompleteAudioFileInfoIds)

    processThreads = []
    for audioFileId in incompleteAudioFileInfoIds:
        processThread = Thread(target=processIncompleteAudio, args=(audioFileId,))
        processThread.start()
        processThreads.append(processThread)

    for processThread in processThreads:
        processThread.join()


def getRemoteFile(url, localFilePath):
    if url.startswith("s3://"):
        bucket, key = url[len("s3://"):].split('/', 1)
        session = boto3.session.Session()
        s3 = session.resource('s3')
        s3.Bucket(bucket).download_file(key, localFilePath)
    else:
        response = requests.get(url.strip(), stream=True)
        with open(localFilePath, 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
            print "done download file ", url
            del response


def uploadFileS3(localLocation, s3Name):
    session = boto3.session.Session()
    s3 = session.resource('s3')
    if '/' in settings.S3_AUDIO_BUCKET:
        bucket, path = settings.S3_AUDIO_BUCKET.split('/')
    else:
        bucket, path = settings.S3_AUDIO_BUCKET, ""
    keyname = os.path.join(path, s3Name)
    s3.meta.client.upload_file(localLocation, bucket, keyname)
    return "s3://{}/keyname".format(bucket, keyname)


def guaranteeFileAtLocation(audioFileInfo):
    name = str(uuid.uuid4()) + ".mp3"
    #get the directory of the this python script
    dir_path = os.path.dirname(os.path.realpath(__file__))
    fileDirectory = os.path.join(dir_path, 'files')
    if not os.path.exists(fileDirectory):
        try:
            os.makedirs(fileDirectory)
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    localLocation = os.path.join(fileDirectory, name)

    if audioFileInfo.s3_location_path is None:
        getRemoteFile(audioFileInfo.file_url, localLocation)
        audioFileInfo.s3_location_path = uploadFileS3(localLocation, name)
    else:
        getRemoteFile(audioFileInfo.s3_location_path, localLocation)

    return localLocation


def setTranscript(audioFileInfo, localLocation):
    if audioFileInfo.voicebase_raw_response is None:
        # call api to get transcript
        # save transcript to db
        
        #getting the api happens in three steps
        # 1. submitting the audio file job
        vb = VoiceBase()
        company = Company.objects.values_list('custom_keyword', 'company_name')[0]
        if company is not None:
            customWords = str(company[0]).split(',')
            vocabName = str(company[1])
            vb.uploadVocab(vocabName, customWords)
            vb.uploadCustomKeyword(vocabName, customWords)
            mediaId = vb.postMediaCurl(localLocation, vocabName, vocabName)['mediaId']
        else:
            mediaId = vb.postMediaCurl(localLocation)['mediaId']
        print "posted media job"

        #2 querying the system to see if the job is done
        #resposse looks like [u'status', u'mediaId', u'topics', u'dateCreated', u'job', u'keywords', u'transcripts', u'metadata']
        jobStatus, response = vb.getMediaJob(mediaId)
        while jobStatus != 'finished':
            print "job status is", jobStatus
            print "waiting for job to finish"
            #todo if they change the api so it returns a work other than finished we'll infinite loop
            time.sleep(60) #wait for a minute then try again
            jobStatus, response = vb.getMediaJob(mediaId)

        # 3. save the relevant part of the transcript to the db
        audioFileInfo.voicebase_raw_response = response
        #print audioFileInfo.voicebase_raw_response
        
        print "transcript done"
    else:
        print "Transcript already found for audio id {}".format(audioFileInfo.id)


def setDiarization(audioFileInfo, localLocation):
    if audioFileInfo.IBM_raw_response is None:
        # call api to get diarization
        # save transcript to db
        watson = Watson()
        print "converting mp3 to flac"
        flacPath = watson.convertMp3ToFlac(localLocation)

        response = watson.postMedia(flacPath)
        audioFileInfo.IBM_raw_response = response

        os.remove(flacPath)
        os.remove(localLocation)
    else:
        print "diarization already exists for audio id {}".format(audioFileInfo.id)


class WordDescriptor(object):

    @staticmethod
    def fromJson(jsonObject):
        if jsonObject is None:
            return None

        return WordDescriptor(jsonObject["c"], jsonObject["s"], jsonObject["e"], jsonObject["c"], jsonObject["w"], jsonObject.get("m", None))

    def __init__(self, position, startTimeMs, endTimeMs, confidence, word, metaWordInfo=None):
        self.position = position
        self.startTimeMs = startTimeMs
        self.endTimeMs = endTimeMs
        self.confidence = confidence
        self.word = word
        self.metaWordInfo = metaWordInfo

    # used to be backwards compatible
    # this should probably log into json deserailzation format
    def toJson(self):
        descriptor = { "c" : self.confidence, "e": self.endTimeMs, "p": self.position, "s": self.startTimeMs, "w":self.word}
        if self.metaWordInfo is not None:
             descriptor["m"] = self.metaWordInfo
        return descriptor

    
class Transcript(object):

    def __init__(self):
        self.words = []

    def addWord(wordDescriptor):
        #validate its a good word descriptor
        self.words.add(wordDescriptor.toJson())


class Diarization(object):

    def __init__(self):
        self.lines = []

    #seconds
    def addLine(self, speakerId, startTime, endTime, words):
        #validate each first three are numbers
        #words is a string of WordDescriptors
        line = { "duration" : endTime-startTime, "interval" : [startTime, endTime], "speaker" : speakerId, "word":[x.toJson() for x in words]}
        self.lines.append(line)

    #given time in ms gets the speaker
    def givenTimeGetSpeaker(self, timeSec):
        time = float(timeSec)
        for line in self.lines:
            startTime = line["interval"][0]
            endTime = line["interval"][1]
            if startTime <= time and time <= endTime:
                return line["speaker"]

        raise Exception("how did we get here")

    def toJson(self):
        return { "lines": self.lines }


class Keywords(object):

    def __init__(self):
        # organized first by speaker, then by category
        # essentially allows keyswords["a"]["b"]["c"].add(d) without having to worry about things existing
        self.keywords = {}

    def addKeyword(self, speakerId, category, word, time):
        self._addKeyword(speakerId, category, word, time)
        self._addKeyword(speakerId, 'All', word, time)

    def _addKeyword(self, speakerId, category, word,  time):

        #make sure we won't get a key error 
        if not speakerId in self.keywords:
            self.keywords[speakerId] = {}
        if not category in self.keywords[speakerId]:
            self.keywords[speakerId][category] = {}
        if not word in self.keywords[speakerId][category]:
            self.keywords[speakerId][category][word] = []

        # actualy add the word
        self.keywords[speakerId][category][word].append(time)

    def calculateSpeakerAndAddKeyword(self, category, word, time, diarizationObject):
        speakerId = diarizationObject.givenTimeGetSpeaker(time)
        self.addKeyword(speakerId, category, word, time)

    #this is gross we should look into a fix
    def toJson(self):
        return { "keywords": self.keywords }


def iteratorGetNext(iterator):
    try: 
        return iterator.next()
    except StopIteration as e:
        return None


class SpeakerLabel(object):

    # start time is inclusvie and end time is exclusive
    def __init__(self, start, speakerId, end):
        self.startTimeMs = start
        self.speakerId = speakerId
        self.endTimeMs = end

    @staticmethod
    def fromJson(jsonObject):
        if jsonObject is None:
            return None

        return SpeakerLabel(jsonObject["from"]*1000, jsonObject["speaker"], jsonObject["to"]*1000)
    
    def toJson(self):
        obj = {"start" : self.startTimeMs, "end" : self.endTimeMs, "speakerId" : self.speakerId}
        return obj
        

def postProcessDiarization(words, speakerLabels):
    dira = Diarization()
    speakerIterator = iter(speakerLabels)
    wordIterator = iter(words)
    

    #first preprocess the speakers to get distinct such that two adjacent lables have different speakers
    #right now it labels each word
    denseSpeakerLabels = [] 

    prevSpeaker = None
    speaker = SpeakerLabel.fromJson(iteratorGetNext(speakerIterator))
    assert speaker is not None, "this means there are no speaker labels"
    currentSpeaker =  speaker.speakerId
    currentStartTime = 0

    #currentEndTime = speaker.endTimeMs
    while speaker is not None:
        
        while speaker is not None and speaker.speakerId == currentSpeaker:
            prevSpeaker = speaker
            speaker = SpeakerLabel.fromJson(iteratorGetNext(speakerIterator))
        
        # this is the last element
        if speaker is None:
            #print "previous speaker", prevSpeaker.toJson()
            endTime = prevSpeaker.endTimeMs
            newSpeakerLabel = SpeakerLabel(currentStartTime, currentSpeaker, endTime)
        else:
            #print "speaker", speaker.toJson()
            # we choose the start time of the next speaker so there are no un attributed gaps
            endTime = speaker.startTimeMs
            newSpeakerLabel = SpeakerLabel(currentStartTime, currentSpeaker, endTime)
            currentStartTime = speaker.startTimeMs
            currentSpeaker = speaker.speakerId

        #print "new speaker label to add", newSpeakerLabel.toJson()
        denseSpeakerLabels.append(newSpeakerLabel)

    
    assert speaker is None
    #speaker = SpeakerLabel.fromJson(iteratorGetNext(speakerIterator))
    #now that we have a basis for the speakers
    word = WordDescriptor.fromJson(iteratorGetNext(wordIterator))
    for speaker in denseSpeakerLabels:

        speakerWords = []
        # we have words to parse and the words are still in this speakers sentence
        #print "time of speaker and word", speaker.endTimeMs, word.endTimeMs 
        while word is not None and speaker.endTimeMs > word.endTimeMs:
            speakerWords.append(word)
            word = WordDescriptor.fromJson(iteratorGetNext(wordIterator))

        #the next word does not belong to this speaker label finish up this line
        dira.addLine(speaker.speakerId, speaker.startTimeMs/1000, speaker.endTimeMs/1000, speakerWords)

    if word is not None:
        print "next would be", iteratorGetNext(wordIterator)
        print "next would be", iteratorGetNext(wordIterator)
        print "next would be",iteratorGetNext(wordIterator)
        print denseSpeakerLabels[-1].toJson()
        raise Exception("speaker labels were finished, but there were still words to proceess")
    return dira


def postProcessKeywords(diarization, raw_transcript):
    #the keywords json object, has three relevant keywords section
    postProcesssedKeywords = Keywords()
    
    # 1 
    keywordDescriptiors = raw_transcript["media"]["keywords"]["latest"]["words"]
    for descriptor in keywordDescriptiors:    
        keyword = descriptor["name"]
        try: 
            for time in descriptor["t"]["unknown"]:
                postProcesssedKeywords.calculateSpeakerAndAddKeyword('Topic Related', keyword, time, diarization)
        except KeyError as e:
            #why does this happen?
            #todo we should add a log here 
            pass

    # 2 these are the keywords that are given to the system
        #these are keywords that we highlight in the important section
    for group in raw_transcript["media"]["keywords"]["latest"]["groups"]:
        for descriptor in group["keywords"]:
            keyword = descriptor["name"]
            try: 
                times = descriptor["t"]["unknown"]
                for time in times:
                    postProcesssedKeywords.calculateSpeakerAndAddKeyword('Important', keyword, time, diarization)
            except KeyError as e:
                #why does this happen?
                #todo we should add a log here 
                pass

    # 3 are words that pre-categorized by topic
        # raw
    for topic in raw_transcript["media"]["topics"]["latest"]["topics"]:
        category = topic["name"]

        for wordDescriptor in topic["keywords"]:
            keyword = wordDescriptor["name"]
            try:
                for time in wordDescriptor["t"]["unknown"]:
                    postProcesssedKeywords.calculateSpeakerAndAddKeyword(category, keyword, time, diarization)
            except KeyError as e:
                #why does this happen?
                #todo we should add a log here 
                pass

    return postProcesssedKeywords


def postProcesses(rawTranscript, rawDiarization):
    tran = rawTranscript["media"]["transcripts"]["latest"]["words"]
    #diarization 
    #iterate through bot speaker lables and transcript words at the same time
    #when word start time is greater than speaer label to time end that line
    #todo consider when its back to back with the same speaker what do we do

    dira = postProcessDiarization(tran, rawDiarization["speaker_labels"])
    keywords = postProcessKeywords(dira, rawTranscript)
    return (tran, dira, keywords)


def setPostProcessedTranscript(audioFileInfo):
    if audioFileInfo.keyword is None or audioFileInfo.transcript is None or audioFileInfo.diarization is None:
        # call function to get postProcessed 
        print "creating post procesed: transcript, diarization, keywords"
        print type(audioFileInfo.voicebase_raw_response)
        (transcript, diarization, keywords) = postProcesses(audioFileInfo.voicebase_raw_response,
                                                            audioFileInfo.IBM_raw_response)
        audioFileInfo.transcript = { 'transcript': transcript }
        audioFileInfo.keyword = keywords.toJson()
        audioFileInfo.diarization = diarization.toJson()
        audioFileInfo.save()
        # save transcript to db
        #print transcript
        #print diarization.toJson()
    else:
        print "keyword is", audioFileInfo.keyword.keys()


if __name__ == "__main__":
    sleepBetweenDBchecksInSeconds = 30
    #loop to check if there are any updates that need to be addressed in the db
    while True:
        processIncompleteAudioFiles()
        #sleep so we don't hammer the system
        time.sleep(sleepBetweenDBchecksInSeconds)
