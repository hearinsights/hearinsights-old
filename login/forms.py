#log/forms.py
from django.contrib.auth.forms import AuthenticationForm
from django import forms
from django.forms import ModelForm, Textarea
# If you don't do this you cannot use Bootstrap CSS
from login.models import Company


class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30, 
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
    password = forms.CharField(label="Password", max_length=30, 
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'password'}))


class Custom_keywordForm(forms.ModelForm):
    custom_keyword = forms.CharField(widget=forms.Textarea(attrs={'class':'form-control', 'rows':'3'}))
    class Meta:
        model = Company
        fields = ('custom_keyword',)


TITLE_CHOICES = (
    ('Discovery', 'Discovery'),
    ('Technical Review', 'Technical Review'),
    ('Pricing', 'Pricing'),
)

class AudioForm(forms.Form):
  file_url = forms.URLField(widget=forms.URLInput(attrs={
                        'placeholder': 'Audio URL',
                    }))
  client_stage = forms.ChoiceField(choices=TITLE_CHOICES)

  client_name = forms.CharField(max_length=100)




    





