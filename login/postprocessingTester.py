import json

class WordDescriptor(object):

    @staticmethod
    def fromJson(jsonObject):
        if jsonObject is None:
            return None

        return WordDescriptor(jsonObject["c"], jsonObject["s"], jsonObject["e"], jsonObject["c"], jsonObject["w"], jsonObject.get("m", None))

    def __init__(self, position, startTimeMs, endTimeMs, confidence, word, metaWordInfo=None):
        self.position = position
        self.startTimeMs = startTimeMs
        self.endTimeMs = endTimeMs
        self.confidence = confidence
        self.word = word
        self.metaWordInfo = metaWordInfo

    # used to be backwards compatible
    # this should probably log into json deserailzation format
    def toJson(self):
        descriptor = { "c" : self.confidence, "e": self.endTimeMs, "p": self.position, "s": self.startTimeMs, "w":self.word}
        if self.metaWordInfo is not None:
             descriptor["m"] = self.metaWordInfo
        return descriptor
        
class Transcript(object):

    def __init__(self):
        self.words = []

    def addWord(wordDescriptor):
        #validate its a good word descriptor
        self.words.add(wordDescriptor.toJson())

class Diralization(object):

    def __init__(self):
        self.lines = []

    #seconds
    def addLine(self, speakerId, startTime, endTime, words):
        #validate each first three are numbers
        #words is a string of WordDescriptors
        line = { "duration" : endTime-startTime, "interval" : [startTime, endTime], "speaker" : speakerId, "word":[x.toJson() for x in words]}
        self.lines.append(line)

    #given time in ms gets the speaker
    def givenTimeGetSpeaker(self, timeSec):
        time = float(timeSec)
        for line in self.lines:
            startTime = line["interval"][0]
            endTime = line["interval"][1]
            if startTime <= time and time <= endTime:
                return line["speaker"]

        raise Exception("how did we get here")

class Keywords(object):

    def __init__(self):
        # organized first by speaker, then by category
        # essentially allows keyswords["a"]["b"]["c"].add(d) without having to worry about things existing
        self.keywords = {}

    def addKeyword(self, speakerId, category, word, time):
        self._addKeyword(speakerId, category, word, time)
        self._addKeyword(speakerId, 'All', word, time)

    def _addKeyword(self, speakerId, category, word,  time):

        #make sure we won't get a key error 
        if not speakerId in self.keywords:
            self.keywords[speakerId] = {}
        if not category in self.keywords[speakerId]:
            self.keywords[speakerId][category] = {}
        if not word in self.keywords[speakerId][category]:
            self.keywords[speakerId][category][word] = []

        # actualy add the word
        self.keywords[speakerId][category][word].append(time)

    def calculateSpeakerAndAddKeyword(self, category, word, time, diralizationObject):
        speakerId = diralizationObject.givenTimeGetSpeaker(time)
        self.addKeyword(speakerId, category, word, time)

    #this is gross we should look into a fix
    def toJson(self):
        return self.keywords



def iteratorGetNext(iterator):
    try: 
        return iterator.next()
    except StopIteration as e:
        return None

class SpeakerLabel(object):

    # start time is inclusvie and end time is exclusive
    def __init__(self, start, speakerId, end):
        self.startTimeMs = start
        self.speakerId = speakerId
        self.endTimeMs = end

    @staticmethod
    def fromJson(jsonObject):
        if jsonObject is None:
            return None

        return SpeakerLabel(jsonObject["from"]*1000, jsonObject["speaker"], jsonObject["to"]*1000)
    
    def toJson(self):
        obj = {"start" : self.startTimeMs, "end" : self.endTimeMs, "speakerId" : self.speakerId}
        return obj
        

def postProcessDiralization(words, speakerLabels):
    dira = Diralization()
    speakerIterator = iter(speakerLabels)
    wordIterator = iter(words)
    

    #first preprocess the speakers to get distinct such that two adjacent lables have different speakers
    #right now it labels each word
    denseSpeakerLabels = [] 

    prevSpeaker = None
    speaker = SpeakerLabel.fromJson(iteratorGetNext(speakerIterator))
    assert speaker is not None, "this means there are no speaker labels"
    currentSpeaker =  speaker.speakerId
    currentStartTime = 0

    #currentEndTime = speaker.endTimeMs
    while speaker is not None:
        
        while speaker is not None and speaker.speakerId == currentSpeaker:
            prevSpeaker = speaker
            speaker = SpeakerLabel.fromJson(iteratorGetNext(speakerIterator))
        
        # this is the last element
        if speaker is None:
            #print "previous speaker", prevSpeaker.toJson()
            endTime = prevSpeaker.endTimeMs
            newSpeakerLabel = SpeakerLabel(currentStartTime, currentSpeaker, endTime)
        else:
            #print "speaker", speaker.toJson()
            # we choose the start time of the next speaker so there are no un attributed gaps
            endTime = speaker.startTimeMs
            newSpeakerLabel = SpeakerLabel(currentStartTime, currentSpeaker, endTime)
            currentStartTime = speaker.startTimeMs
            currentSpeaker = speaker.speakerId

        #print "new speaker label to add", newSpeakerLabel.toJson()
        denseSpeakerLabels.append(newSpeakerLabel)

    
    assert speaker is None
    #speaker = SpeakerLabel.fromJson(iteratorGetNext(speakerIterator))
    #now that we have a basis for the speakers
    word = WordDescriptor.fromJson(iteratorGetNext(wordIterator))
    for speaker in denseSpeakerLabels:

        speakerWords = []
        # we have words to parse and the words are still in this speakers sentence
        #print "time of speaker and word", speaker.endTimeMs, word.endTimeMs 
        while word is not None and speaker.endTimeMs > word.endTimeMs:
            speakerWords.append(word)
            word = WordDescriptor.fromJson(iteratorGetNext(wordIterator))

        #the next word does not belong to this speaker label finish up this line
        dira.addLine(speaker.speakerId, speaker.startTimeMs/1000, speaker.endTimeMs/1000, speakerWords)

    if word is not None:
        print "next would be", iteratorGetNext(wordIterator)
        print "next would be", iteratorGetNext(wordIterator)
        print "next would be",iteratorGetNext(wordIterator)
        print denseSpeakerLabels[-1].toJson()
        raise Exception("speaker labels were finished, but there were still words to proceess")
    return dira

def postProcessKeywords(diralization, raw_transcript):
    #the keywords json object, has three relevant keywords section
    postProcesssedKeywords = Keywords()
    
    # 1 
    keywordDescriptiors = raw_transcript["media"]["keywords"]["latest"]["words"]
    for descriptor in keywordDescriptiors:    
        keyword = descriptor["name"]
        for time in descriptor["t"]["unknown"]:
            postProcesssedKeywords.calculateSpeakerAndAddKeyword('Topic Related', keyword, time, diralization)

    # 2 these are the keywords that are given to the system
        #these are keywords that we highlight in the important setion
    for group in raw_transcript["media"]["keywords"]["latest"]["groups"]:
        for descriptor in group["keywords"]:
            keyword = item["name"]
            try: 
                times = item["t"]["unknown"] 
                for time in times:
                    postProcesssedKeywords.calculateSpeakerAndAddKeyword('Important', keyword, time, diralization)
            except:
                #why does this happen?
                #todo we should add a log here 
	        pass

    # 3 are words that pre-categorized by topic
        # raw
    for topic in raw_transcript["media"]["topics"]["latest"]["topics"]:
        category = topic["name"]

        for wordDescriptor in topic["keywords"]:
            keyword = wordDescriptor["name"]
            try:
                for time in wordDescriptor["t"]["unknown"]:
                    postProcesssedKeywords.calculateSpeakerAndAddKeyword(category, keyword, time, diralization)
            except:
                #why does this happen?
                #todo we should add a log here 
                pass

    return postProcesssedKeywords

def postProcesses(rawTranscript, rawDiralization):
    #tran = Transcript()

    tran = rawTranscript["media"]["transcripts"]["latest"]["words"]
    #diralization 
    #iterate through bot speaker lables and transcript words at the same time
    #when word start time is greater than speaer label to time end that line
    #todo consider when its back to back with the same speaker what do we do

    dira = postProcessDiralization(tran, rawDiralization["speaker_labels"])
    keywords = postProcessKeywords(dira, rawTranscript)


with open('API_references/sampleConvoTranscript.json', 'r') as content_file:
        rawT = json.loads(content_file.read())

with open('API_references/sampleConvoDiralization.json', 'r') as content_file:
        rawD = json.loads(content_file.read())

postProcesses(rawT, rawD)
