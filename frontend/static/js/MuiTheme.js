import getMuiTheme from 'material-ui/styles/getMuiTheme'


const MuiTheme = getMuiTheme()

export default MuiTheme
