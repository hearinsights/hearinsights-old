import * as Redux from 'redux'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'


const INITIAL_STATE = {}

const defaultReducer = (state=INITIAL_STATE, action) => {
  switch (action.type) {
    default:
      return state
  }
}

export default Redux.createStore(
  defaultReducer,
  INITIAL_STATE,
  Redux.applyMiddleware(thunk, createLogger())
)
