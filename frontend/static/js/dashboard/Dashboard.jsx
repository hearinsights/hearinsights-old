import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import MuiTheme from '../MuiTheme'
import RaisedButton from 'material-ui/RaisedButton';


class Dashboard extends React.Component {

  render() {
    return (
      <MuiThemeProvider muiTheme={MuiTheme}>
        <div>
        <RaisedButton label="Test Button" />
        </div>
      </MuiThemeProvider>
    )
  }
}

export default Dashboard
