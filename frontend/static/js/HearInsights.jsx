import { Provider } from 'react-redux'
import { Router, Route, Redirect, browserHistory } from 'react-router'

import Store from './dashboard/redux/Store'
import Dashboard from './dashboard/Dashboard'


class HearInsights extends React.Component {
  render() {
    return (
      <Provider store={Store}>
        <Router history={browserHistory}>
          <Redirect from='/' to='dashboard/' />
          <Route path='dashboard/*' component={Dashboard} />
        </Router>
      </Provider>
    )
  }
}

export default HearInsights
