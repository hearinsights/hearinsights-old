import injectTapEventPlugin from 'react-tap-event-plugin';
import { AppContainer } from 'react-hot-loader'

import HearInsights from './HearInsights'


injectTapEventPlugin();

const hearInsightsElement = document.getElementById('HearInsights')

ReactDOM.render(
  <AppContainer>
    <HearInsights />
  </AppContainer>,
  hearInsightsElement
)

if (module.hot) {
  module.hot.accept('./HearInsights', () => {
    const HotApp = require('./HearInsights').default;
    ReactDOM.render(
      <AppContainer>
        <HotApp />
      </AppContainer>,
      hearInsightsElement
    )
  })
}