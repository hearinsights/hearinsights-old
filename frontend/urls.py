from django.conf.urls import url

from frontend.views import dashboard


urlpatterns = [
  url(r'^$', dashboard),
  url(r'^dashboard/.*$', dashboard),
]