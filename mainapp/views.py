from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.db import transaction
from login.models import Audio, Client 
from django.contrib.auth.models import User
from django.shortcuts import render
from mainapp.forms import SearchForm, ClientForm, RepForm 

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json


def search_results(request, audio_list, user_list, client_list, search_form, rep_form, client_form):
	page = request.GET.get('page', 1)
	paginator = Paginator(audio_list, 100)
	try:
		audios = paginator.page(page)
	except PageNotAnInteger:
		audios = paginator.page(1)
	except EmptyPage:
		audios = paginator.page(paginator.num_pages)
	return render(request, 'search_results.html', {'user_list': user_list,'client_list': client_list, 'audios':audios, 'SearchForm':search_form, 'RepForm': rep_form, 'ClientForm': client_form})


@login_required(login_url="/login/")
def index(request):
	users = User.objects.all()
	user_list = []
	
	for user in users:
		user_list.append((user.username, user.get_full_name()))
	client_list = list(Client.objects.values_list('client_name', flat=True))

	if request.method == 'POST':
		search_form = SearchForm(request.POST)
		rep_form = RepForm(request.POST)
		client_form = ClientForm(request.POST)
		if search_form.is_valid() and rep_form.is_valid() and client_form.is_valid():			
			keyword_input = str(search_form.cleaned_data['search_query'])
			rep_input = rep_form.cleaned_data['rep']
			client_input = client_form.cleaned_data['client']

			command = ''

			if keyword_input != '':
				keyword_input = keyword_input.split(' ')
				command = command + "hey->>'w' ilike any(array" + str(keyword_input) +")"

			if client_input != '':
				client = Client.objects.filter(client_name=client_input)
				if command != '':
					command = command + ' AND client_id=' + str(client)
				else:
					command = command + 'client_id=' + str(client)

			if rep_input != '':
				user = User.objects.filter(username=rep_input)
				if command != '':
					command = command + ' AND user_id=' + str(user)
				else:
					command = command + 'user_id=' + str(user)

			final_command = """
				SELECT id 
				FROM login_audio, jsonb_array_elements(transcript->'transcript') as hey 
				WHERE(""" + command + """) 
				GROUP BY id
				""" 
			audio_list = list(Audio.objects.raw(final_command))

			print '===========audio_list 2=========='
			print audio_list

			user_list = json.dumps(user_list)
			client_list = json.dumps(client_list)

			search_form = SearchForm()
			rep_form = RepForm()
			client_form = ClientForm()

#			return render(request, 'index.html', {'user_list': user_list,'client_list': client_list, 'audios':audios, 'SearchForm':search_form, 'RepForm': rep_form, 'ClientForm': client_form})
			return search_results(request, audio_list, user_list, client_list, search_form, rep_form, client_form)
	else:

		audio_list = Audio.objects.all()

		page = request.GET.get('page', 1)
		paginator = Paginator(audio_list, 20)
		try:
			audios = paginator.page(page)
		except PageNotAnInteger:
			audios = paginator.page(1)
		except EmptyPage:
			audios = paginator.page(paginator.num_pages)

		user_list = json.dumps(user_list)
		client_list = json.dumps(client_list)

		search_form = SearchForm()
		rep_form = RepForm()
		client_form = ClientForm()

	return render(request, 'index.html', {'user_list': user_list,'client_list': client_list, 'audios':audios, 'SearchForm':search_form, 'RepForm': rep_form, 'ClientForm': client_form})

