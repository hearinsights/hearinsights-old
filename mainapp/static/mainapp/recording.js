$(document).ready(
  function() {

    var player = videojs('audio').ready(function () {
      var currentLine = document.getElementById("line0")
      this.on('timeupdate', function () {
        var line_idx
        var interval
        var start
        var end
        for (line_idx in diarization) {
          interval = diarization[line_idx].interval
          start = interval[0]
          end = interval[1]

          if (start <= this.currentTime() && this.currentTime() <= end) {
            currentLine.style.borderStyle = 'none'
            currentLine = document.getElementById("line" + line_idx)
            currentLine.style.borderColor = 'black'
            currentLine.style.borderStyle = 'solid'
          }
        }
      })
    })

    //load the marker plugin
    player.markers({
      markerTip: {
        display: true,
        text: function(marker) {
          return marker.text;
        }
      }
    });


    $('button#calculate').bind('click', function() {
      $( "#result" ).empty();
      audio_id = $("#audio").attr("data-id"); 
      $.getJSON('/_add_numbers', {
        a: $('input[name="a"]').val(),
        b: audio_id
      }, function(data) {
        var tmp = [];
        $.each(data.result, function( index, value ) {
          value = value.map(function(x){ return x / 1000; });
          // console.log(value);
          tmp = value;
          frequency = value.length;
          command = '<li><a class="key" href="#" data-time="' + value.join()+ '" speakers="" style="display:inline-block;">' +index+ '</a><span> ('+frequency+') </span></li>'
          $("#result").append(command);
          addTimeStampsOnClick();
        });
        if (tmp.length === 0) {
          $("#result").append('<div>No matching result</div>');
        };
      });
    });

    $.each(keyword, function(speaker_id) {
      var displaySpeaker = parseInt(speaker_id) + 1
      $("#checkbox-speaker").append('<label class="checkbox-inline"><input name="speaker-' + speaker_id + '" type="checkbox" value="' + speaker_id + '" checked>Speaker ' + displaySpeaker + '</label>')
    })

    var renderKeywords = function(speaker_list) {
      $.each(speaker_list, function(id, speaker_id) {
      var speakerKeyword = keyword[speaker_id]
      $.each(speakerKeyword, function(topic, topic_value){
        //console.log(index);

        var getKeywords = function(topicKeywords) {
          var tmp = ''
          $.each(topicKeywords, function(value, time) {
            if (time.length > 0) {
              // console.log(value['keyword'])
              tmp += '<li><a class="key" href="#" data-time="' + time.join() +'" speakers="" style="display:inline-block;">'+value+'</a><span>('+time.length+')</span></li>'
            }
          })
          return tmp
        }

        var topicSanitized = topic.replace(/[\s()]/g,"-")
        var topicDoesNotExist = $('a[href$="' + topicSanitized + '"]').length === 0
        if (topicDoesNotExist) {
          if (topic === 'Important') {
            $("#important-keyword-topic").append('<a href="#'+ topicSanitized +'" data-toggle="tab">'+ topic +'</a>');
          } else if (topic === 'All') {
            $("#all-keyword-topic").append('<a href="#'+ topicSanitized +'" data-toggle="tab">'+ topic +'</a>');
          } else {
            $("#keyword-topic-list").append('<li><a href="#'+ topicSanitized +'" data-toggle="tab">'+ topic+ '</a></li>');
          }

          $("div#" + topicSanitized).empty()
          var tmp = ''
          $.each(speaker_list, function(id, speaker_id) {
            if (topic in keyword[speaker_id]) {
              tmp += getKeywords(keyword[speaker_id][topic])
            }
          })
          $("#keyword-content").append('<div class="tab-pane" id="'+ topicSanitized +'"><ul data-keyword-topic="'+ topicSanitized +'">'+ tmp +'</ul></div>')
        }
      })
      })
    }

    var all = []
    $.each(keyword, function(speaker_id) {
      all.push(speaker_id)
    })
    renderKeywords(all)


    var msToTime = function(duration) {
      var milliseconds = parseInt((duration%1000)/100)
          , seconds = parseInt((duration/1000)%60)
          , minutes = parseInt((duration/(1000*60))%60)
          , hours = parseInt((duration/(1000*60*60))%24);

      hours = (hours < 10) ? "0" + hours : hours;
      minutes = (minutes < 10) ? "0" + minutes : minutes;
      seconds = (seconds < 10) ? "0" + seconds : seconds;

      return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
    }


    var sToTime = function(duration) {
      var   seconds = parseInt((duration)%60)
          , minutes = parseInt((duration/(60))%60)
          , hours = parseInt((duration/(60*60))%24);

      hours = (hours < 10) ? "0" + hours : hours;
      minutes = (minutes < 10) ? "0" + minutes : minutes;
      seconds = (seconds < 10) ? "0" + seconds : seconds;

      return hours + ":" + minutes + ":" + seconds;
    }

    diarization = $.grep(diarization, function(line) {
      return line.word.length !== 0
    })

    var renderedDiarizationIndex = 0
    $.each(diarization, function(line_index, line) {
      if (line_index < renderedDiarizationIndex) {
        return
      }

      duration = line['duration']
      interval = line['interval']
      start_time = line['interval'][0]
      converted_start_time = sToTime(line['interval'][0])
      speaker = "Speaker_" + (parseInt(line['speaker']) + 1)
      word = line['word']

      var end_index = line_index
      while (end_index < diarization.length && line.speaker === diarization[end_index].speaker) {
        end_index += 1
      }

      renderedDiarizationIndex = end_index


      // console.log('============time============');
      // console.log('========================');
      // console.log(line);
      // console.log(duration);
      // console.log(interval);
      // console.log(speaker);
      // console.log(word);

      var tmp = ''
      for (var i=line_index; i<end_index; i++) {
        word = diarization[i].word
        $.each(word, function(word_index, word) {
          converted_word_start_time = word['s']/1000.0
          w = word['w']
          tmp += '<span><a class="key" href="#" data-time="'+ converted_word_start_time +'" style="display:inline-block;padding-left: 3px;">'+w+'</a></span>'
        });
        if (i !== end_index - 1) {
          tmp += '<br /><br />'
        }
      }

      if (tmp !== '') {
        $("#transcript-content").append('<div id="line'+ line_index +'" class="col-md-12 '+ speaker +'"><div class="transcript-info col-md-2"><li>'+ speaker + '</li><li><a class="key" href="#" data-time="' + start_time +'" speakers="" style="display:inline-block;padding-right: 3px;">'+converted_start_time+'</a></li></div><div class="col-md-9">'+tmp+'</div></div>')
      }
    });


    var addTimeStampsOnClick = function() {
      $(".key").click(function(event) {
        $eventTarget = $(event.target);
        try {
          timeStamps = $eventTarget.data('time').split(',');
          keyWord = $eventTarget.context.innerText;
          var keyWordTimeStamps = [];
          timeStamps.forEach(function(timeStamp){
            keyWordTimeStamps.push({time: Number(timeStamp), text: keyWord})
          });
          player.markers.reset(keyWordTimeStamps);
          player.markers.next();
        } catch (err) {
          timeStamps = [$eventTarget.data('time')]
          keyWord = $eventTarget.context.innerText;
          var keyWordTimeStamps = [];
          timeStamps.forEach(function(timeStamp) {
            keyWordTimeStamps.push({time: Number(timeStamp), text: keyWord})
          });
          player.markers.reset(keyWordTimeStamps);
          player.markers.next();
        }
      })
    };
    addTimeStampsOnClick();

    $('#checkbox-speaker').on('change', function(e) {
      e.preventDefault()
      var data = $('#checkbox-speaker').serializeArray()
      $("#keyword-topic-list").empty()
      $("#keyword-topic-list").append('<li class="active" id="important-keyword-topic"></li>')
      $("#keyword-topic-list").append('<li id="all-keyword-topic"></li>')
      $("#keyword-content").empty()
      var speaker_list = []
      for (var i = 0; i < data.length; i++) {
        speaker_list.push(data[i].value)
      }
      renderKeywords(speaker_list)
    })

  }
);