// initialize video.js
var player = videojs('my_video_1');

//load the marker plugin
player.markers({
   markerTip:{
      display: true,
      text: function(marker) {
         return "This is a break: " + marker.text;
      }
   },
   breakOverlay:{
      display: true,
      displayTime: 3,
      text: function(marker) {
         return "This is an break overlay: " + marker.text;
      }
   },
   onMarkerReached: function(marker) {
      $('.event-list').append("<div>marker reached: " + marker.text + "</div>");

   },
   onMarkerClick: function(marker){
      $('.event-list').append("<div>marker clicked: " + marker.text + "</div>");

   },
   markers: [
      {
         time: 9.5,
         text: "this"
      },
      {
         time: 16,
         text: "is"
      },
      {
         time: 23.6,
         text: "so"
      },
      {
         time: 28,
         text: "cool"
      },
      {
         time: 36,
         text: ":)"
      }
   ]
});

$(".next").click(function(){
   player.markers.next();
});
$(".prev").click(function(){
   player.markers.prev();
});
$(".remove").click(function(){
   player.markers.remove([0,1]);
})
$(".add").click(function(){
   player.markers.add([{
         time: 25,
         text: "I'm NEW"
      }]);
});
$(".updateTime").click(function(){
   var markers = player.markers.getMarkers();
   for (var i = 0; i < markers.length; i++) {
      markers[i].time += 1;
   }
   player.markers.updateTime();
});



$('.vjs-big-play-button').remove();
$('.vjs-volume-menu-button').remove();
$('.vjs-loading-spinner').remove();
$('.vjs-live-control').remove();
$('.vjs-chapters-button').remove();
$('.vjs-descriptions-button').remove();
$('.vjs-subtitles-button').remove();
$('.vjs-captions-button').remove();
$('.vjs-fullscreen-control').remove();

$(".reset").click(function(){
   player.markers.reset([{
         time: 40,
         text: "I'm NEW"
      },
      {
         time:20,
         text: "Brand new"
      }]);
});
$(".destroy").click(function(){
   player.markers.destroy();
})