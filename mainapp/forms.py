#log/forms.py
from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm


class SearchForm(forms.Form):
	search_query = forms.CharField(max_length=100, label='SearchForm')
#  client_name = forms.CharField(max_length=100)
#  user_name = forms.CharField(max_length=100)

class RepForm(forms.Form):
	rep = forms.CharField(max_length=100, required=False)
#    rep = forms.ModelChoiceField(queryset=User.objects.all().order_by('username'))

class ClientForm(forms.Form):
	client = forms.CharField(max_length=100, required=False)

