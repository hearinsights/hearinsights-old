from django.contrib import admin


from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from .models import Employee

# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class EmployeeInline(admin.StackedInline):
    model = Employee
    can_delete = False
    verbose_name_plural = 'employee'

#This part extend an Company field under each user in admin panel
# Define a new User admin
#This part extend an Company field under each user in admin panel
class UserAdmin(BaseUserAdmin):
    inlines = (EmployeeInline, )
# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

#log/forms.py
from django.contrib.auth.forms import AuthenticationForm 
from django import forms
