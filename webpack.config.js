var path = require('path')
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')


module.exports = {
  context: __dirname,

  // entry: {
  //   base: './static/js/base',
  //   dashboard: './frontend/static/js/index',
  // },

  entry: {
    base: [
      'react-hot-loader/patch',
      'webpack-dev-server/client?http://localhost:3000',
      'webpack/hot/only-dev-server',
      './static/js/base',
    ],
    dashboard: [
      'react-hot-loader/patch',
      'webpack-dev-server/client?http://localhost:3000',
      'webpack/hot/only-dev-server',
      './frontend/static/js/index',
    ]
  },

  output: {
    path: path.resolve('./static/bundles/'),
    filename: "[name]-[hash].js",
    publicPath: 'http://localhost:3000/static/bundles/'
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new BundleTracker({filename: './webpack-stats.json'}),
    new webpack.ProvidePlugin({
      '_' : 'lodash',
      'React': 'react',
      'ReactDOM': 'react-dom',
    })
  ],

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loaders: ['babel']
      }
    ]
  },

  resolve: {
    modulesDirectories: ['node_modules', 'bower_components'],
    extensions: ['', '.js', '.jsx']
  },

}